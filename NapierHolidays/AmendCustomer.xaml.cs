﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the AmendCustomer window which is opened from the MainWindow. 
    *  
    *  It allows the user to Amend or Delete a Customer's details.
    *  
    *  Last Updated 28/11/16 20:02
    */

    public partial class AmendCustomer : Window
    {
        public AmendCustomer()
        {
            InitializeComponent();
            fill_CustCombo();
        }

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command;

        void fill_CustCombo() // This method will fill the custCombo with Customer names from the database. This will identify which customer the booking is for.  
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Customer"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string Name = reader.GetString(1); // Column 1 indicates the Customer Name column in the SQLite database
                custCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        private void custCombo_DropDownClosed(object sender, EventArgs e) // This method will allow the user to choose a customer to amend or delete using the custCombo combo box. 
        {
            if (custCombo.Text == String.Empty) // If the combobox is closed but missing string, show message
            {
                MessageBox.Show("Please select a customer from the dropdown box");
            }
            else // Retrieve the customer details from the database and fill in the boxes
            {
                connection.Open(); // open the connection:
                command = connection.CreateCommand(); // create a new SQL command:
                command.CommandText = "SELECT * FROM Customer WHERE Name = '" + custCombo.Text + "'"; // SQL insert query to database
                SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
                while (reader.Read())
                {
                    int Age = reader.GetInt32(2); // Column 2 indicates the Customer Age column in the database
                    string Address = reader.GetString(3); // Column 3 indicates the Customer Address column in the database
                    nameBox.Text = custCombo.Text;
                    ageBox.Text = Convert.ToString(Age);
                    addressBox.Text = Address;
                }
                connection.Close(); // Close the database connection           
            }
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e) // When Save button is clicked
        {
            Customer amendCust = Customer.Instance; // Create new Customer Intance
            amendCust.OldName = custCombo.Text;
            amendCust.Name = nameBox.Text;
            amendCust.Age = Convert.ToInt32(ageBox.Text);
            amendCust.Address = addressBox.Text;
            amendCust.updateCustomer(); // Method for amending the customer details which are held on the database. See Customer class.
            MessageBox.Show("Customer details saved"); // Message box shown to user
            this.Close(); // Closes the window after customer details have been saved
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            Customer amendCust = Customer.Instance; // Create new Customer Intance
            amendCust.OldName = custCombo.Text;
            amendCust.Name = nameBox.Text;
            amendCust.Age = Convert.ToInt32(ageBox.Text);
            amendCust.Address = addressBox.Text;
            amendCust.deleteCustomer();
            MessageBox.Show("Customer details deleted"); // Message box shown to user
            this.Close(); // Closes the window after customer details have been saved
        }
    }
}
