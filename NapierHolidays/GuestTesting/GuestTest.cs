﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GuestTesting
{
    [TestClass]
    public class GuestTest
    {
        [TestMethod]
        public void Counting_The_Number_Of_Guests_Under_18()
        {
            /* This code will count the number of guests under the age of 18. 
            *  
            *  This code is implemented in the guest class and called in the Invoice window. It's required when calculating the cost of the Guests included. 
            */

            // arrange
            string guest1 = "Included";
            int guest1Age = 17;
            string guest2 = "Included";
            int guest2Age = 15;
            string guest3 = "";
            int guest3Age = 0;
            string guest4 = "";
            int guest4Age = 0;

            int g1 = 0;
            int g2 = 0;
            int g3 = 0;
            int g4 = 0;

            int expected = 2;

            // act
            if (guest1 == String.Empty)
            {
                g1 = 0;
            }
            else
            {
                if (guest1Age < 18)
                {
                    g1 = 1;
                }
                else
                {
                    g1 = 0;
                }
            }
            if (guest2 == String.Empty)
            {
                g2 = 0;
            }
            else
            {
                if (guest2Age < 18)
                {
                    g2 = 1;
                }
                else
                {
                    g2 = 0;
                }
            }
            if (guest3 == String.Empty)
            {
                g3 = 0;
            }
            else
            {
                if (guest3Age < 18)
                {
                    g3 = 1;
                }
                else
                {
                    g3 = 0;
                }
            }
            if (guest4 == String.Empty)
            {
                g4 = 0;
            }
            else
            {
                if (guest4Age < 18)
                {
                    g4 = 1;
                }
                else
                {
                    g4 = 0;
                }
            }

            int actual = (g1+g2+g3+g4);


            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }
    }
}
