﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace NapierHolidays
{

    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the NewCustomer window which is opened from the MainWindow. 
    *  
    *  It allows the user to enter details of a new Customer. The details are then saved to the SQLite database. 
    *  
    *  Last Updated 27/11/16 00:15
    */

    public partial class NewCustomer : Window
    {
        public NewCustomer()
        {
            InitializeComponent();

        }

        private void saveBtn_Click(object sender, RoutedEventArgs e) // When clicked, Customer's name & address will be saved to the database
        {
            if (nameBox.Text == string.Empty || ageBox.Text == string.Empty || addressBox.Text == string.Empty) // Initial check to makle sure both text boxes have been populated
            {
                MessageBox.Show("You must enter both your name and address"); // If empty, user will receive this message
            }
            else
            {
                Customer newCust = Customer.Instance; // new Customer Singleton object 
                newCust.Name = nameBox.Text; // Sets customer name
                newCust.Age = Convert.ToInt32(ageBox.Text);
                newCust.Address = addressBox.Text; // Sets customer address
                newCust.checkforDuplicates(); // This method will check for duplicate customers
                if (newCust.Name == newCust.DuplicateName)
                {
                    MessageBox.Show("This customer already exsists on our database");
                }
                else
                {
                    newCust.AddCust(); // Method in Customer class which adds the customer to the database
                    MessageBox.Show("Customer details saved"); // Message box shown to user
                    this.Close(); // Closes the window after customer details have been saved
                }
            }
        }
    }
}
