﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierHolidays;
using System.Text.RegularExpressions; // For Regex validation

namespace ValidationTesting
{
    [TestClass]
    public class ValidationTest
    {
        [TestMethod]
        public void Does_Regex_Name_Validation_Work()
        {
            /* This code will use Regex (Regular Expressions) to validate a given string. In this case the customer's name. 
            *  
            *  This code is implemented in the NewCustomer, AmendCustomer, NewBooking & AmendBooking windows when validating textboxes
            */

            // Regex validation objects used to validate Guest details
            Regex namePattern = new Regex(@"^[a-zA-Z\s]*\z$"); // Name entered must be alphabetical characters only (a-z & A-Z)

            // arrange
            string custName = "Ben Wilkes";
            int expected = 1;
            int actual;

            // act
            if (namePattern.IsMatch(custName) == false)
            {
                actual = 0; // If name DOES NOT match the validation 0 = false
            }
            else
            {
                actual = 1; // If name DOES match the validation 1 = true
            }


            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }

        [TestMethod]
        public void Does_Regex_Age_Validation_Work()
        {
            /* This code will use Regex (Regular Expressions) to validate a given string. In this case the customer's age. 
            *  
            *  This code is implemented in the NewCustomer, AmendCustomer, NewBooking & AmendBooking windows when validating textboxes
            */

            // Regex validation objects used to validate Guest details
            Regex agePattern = new Regex(@"^[0-9]{1,2}\z$"); // Age entered must be numeric characters only (0-9) and may only be two charaters long

            // arrange
            string custAge = "12";
            int expected = 1;
            int actual;

            // act
            if (agePattern.IsMatch(custAge) == false)
            {
                actual = 0; // If age DOES NOT match the validation 0 = false
            }
            else
            {
                actual = 1; // If age DOES match the validation 1 = true
            }


            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }

        [TestMethod]
        public void Does_Regex_Passport_Validation_Work()
        {
            /* This code will use Regex (Regular Expressions) to validate a given string. In this case the customer's passport number.
            *  
            *  This code is implemented in the NewCustomer, AmendCustomer, NewBooking & AmendBooking windows when validating textboxes
            */

            // Regex validation objects used to validate Guest details
            Regex passportPattern = new Regex(@"^[0-9a-zA-Z]{10}\z$"); // Passport number entered must be in Alpha/Numeric format and must be 10 characters long

            // arrange
            string passportNum = "1234567891";
            int expected = 1;
            int actual;

            // act
            if (passportPattern.IsMatch(passportNum) == false)
            {
                actual = 0; // If passport number DOES NOT match the validation 0 = false
            }
            else
            {
                actual = 1; // If passport number DOES match the validation 1 = true
            }


            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }
    }
}
