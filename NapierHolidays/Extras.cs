﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the Extras class which has been implemented using the Singleton Design Pattern. 
     *  
     * It will hold the properties related to the extras which are added onto a booking. 
    *  
    *  Last Updated 28/11/16 20:02
    */

    class Extras
    {
        private static Extras instance;

        private Extras() { }

        public int BookingRef { get; set; } // auto get set for booking reference
        public string breakfastMeals { get; set; } // auto get set for breakfast meals
        public string eveningMeals { get; set; } // auto get set for evening meals
        public string dietRequirements { get; set; } // auto get set for dietary requirments
        public string carHire { get; set; } // auto get set for Car hire
        public DateTime hireStart { get; set; } // auto get set for Care hire start date
        public DateTime hireEnd { get; set; } // auto get set for Care hire end date
        public string driverName { get; set; } // auto get set for Driver name
        public int hireDays { get; set; } // auto get set for the number to days a car will be hired for

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command; // create a new sql command object

        public static Extras Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Extras();
                }
                return instance;
            }

        }
        public void addExtras() // Method for adding a guest to the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "INSERT INTO Extras (BookingRef, BreakfastMeals, EveningMeals, DietDetails, CarHire, HireStartDate, HireEndDate, HireDays) VALUES ('" + BookingRef + "', '" + breakfastMeals + "', '" + eveningMeals + "', '" + dietRequirements + "', '" + carHire + "', '" + hireStart.ToShortDateString() + "', '" + hireEnd.ToShortDateString() + "', '" + hireDays + "');"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }
    }
}
