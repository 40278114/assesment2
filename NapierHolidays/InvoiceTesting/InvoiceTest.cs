﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierHolidays;

namespace InvoiceTesting
{
    [TestClass]
    public class InvoiceTest
    {
        [TestMethod]
        public void Calculate_Cost_Based_On_Customer_Age()
        {
            /* This code will calculate the cost of a basic booking based on their age
            *  
            *  This code is used in the Invoice window when generating the overal cost
            */

            // arrange
            int numberOfNightsStaying = 7;
            int age = 21;
            int cost = 0;
            int expected = 350;
            
            // act
            if (age < 18)
            {
                cost = 30;
            }
            if (age >= 18)
            {
                cost = 50;
            }
            int actual = numberOfNightsStaying * cost;

            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }

        [TestMethod]
        public void Calculate_Guests_Cost_Based_On_Customer_Age()
        {
            /* This code will calculate the cost of for the Guests included in the booking, again based on the age
            *  
            *  This code is used in the Invoice window when generating the overal cost
            */

            // arrange
            int numberOfGuestsIncluded = 1;
            int numberOfNightsStaying = 3;
            int age = 21;
            int cost = 0;
            int expected = 150;

            // act
            if (numberOfGuestsIncluded >= 1 && age < 18)
            {
                cost = 30;
            }
            if (numberOfGuestsIncluded >= 1 && age >= 18)
            {
                cost = 50;
            }
            int actual = numberOfGuestsIncluded * (numberOfNightsStaying * cost);

            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }

        [TestMethod]
        public void Calculate_Extra_Cost()
        {
            /* This code will calculate the cost for an Extra IF the Extra has been selected. 
             * The number of nights booked, the total number of people staying & the rate of the Extra must already be known at this point.
            *  
            *  This code is used in the Invoice window when generating the overal cost
            */

            // arrange
            string breakfastMeals = "Yes";
            int numberOfPeople = 2;
            int numberOfNightsStaying = 2;
            int extraRate = 5;
            int expected = 20;
            int actual = 0;


            // act
            if (breakfastMeals == "Yes")
            {
                actual = numberOfPeople * (numberOfNightsStaying * extraRate);
            }

            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }
    }
}
