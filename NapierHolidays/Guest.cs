﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the Guest class which implement a Singleton Design Pattern. 
    *  
    *  It will hold the properties for the guest's passport numnber
    *  
    *  Last Updated 28/11/16 20:02
    */

    public class Guest // Inherits Name & Age from Person Class
    {
        private static Guest instance;

        private Guest() { }

        public int BookingRef { get; set; } // auto get set for booking reference
        public string guest1Name { get; set; } // auto get set for Guest 1's name
        public int guest1Age { get; set; } // auto get set for Guest 1's age
        public int guest1PassNum { get; set; } // auto get set for Guest 1's passport number
        public string guest2Name { get; set; } 
        public int guest2Age { get; set; } 
        public int guest2PassNum { get; set; }
        public string guest3Name { get; set; }
        public int guest3Age { get; set; }
        public int guest3PassNum { get; set; }
        public string guest4Name { get; set; }
        public int guest4Age { get; set; }
        public int guest4PassNum { get; set; }

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command; // create a new sql command object

        public static Guest Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Guest();
                }
                return instance;
            }

        }

        public void addGuestDetails() // Method for adding a guest to the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "INSERT INTO Guests (BookingRef, G1Name, G1Age, G1PassNum, G2Name, G2Age, G2PassNum, G3Name, G3Age, G3PassNum, G4Name, G4Age, G4PassNum) VALUES ('" + BookingRef + "', '" + guest1Name + "', '" + guest1Age + "', '" + guest1PassNum + "', '" + guest2Name + "', '" + guest2Age + "', '" + guest2PassNum + "', '" + guest3Name + "', '" + guest3Age + "', '" + guest3PassNum + "' , '" + guest4Name + "', '" + guest4Age + "', '" + guest4PassNum + "');"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }
    }
}
