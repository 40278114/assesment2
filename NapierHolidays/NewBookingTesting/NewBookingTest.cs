﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierHolidays;

namespace NewBookingTesting
{
    [TestClass]
    public class NewBookingTest
    {
        [TestMethod]
        public void Calculate_The_Amount_Of_Days_Between_Two_Dates()
        {
            /* This code will take two dates and calculate the amount of days between each given date 
            *  
            *  This code is used in the NewBooking window when calculating the amount of days a customer is staying
            *  and when calculating the amount of days a car will be hired for  
            */ 

            // arrange
            DateTime arrival = DateTime.Parse("03/12/2016");
            DateTime departure = DateTime.Parse("10/12/2016");
            int expected = 7;

            // act
            TimeSpan ts = departure - arrival; // Calculates the difference in days
            int actual = ts.Days;

            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }

        [TestMethod]
        public void Set_A_Property_Value_If_Something_Else_HasA_Value()
        {
            /* This code will set a value for guestAge - but only if the guestAgeTextblock IS NOT empty
            *  
            *  This code is used in the NewBooking window when saving Guest & Extra details to the database
            */

            // arrange
            string guestAgeTextblock = "Populated";
            int guestAge = 0;
            int expected = 25;

            // act
            if (guestAgeTextblock != String.Empty)
            {
                guestAge = 25;      
            }
            int actual = guestAge;

            // assert
            Assert.AreEqual(expected, actual, 0.001, "Values have not been correctly calculated");
        }
    }
}
