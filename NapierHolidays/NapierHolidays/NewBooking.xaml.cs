﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite; // For SQLite database
using System.Text.RegularExpressions; // For Regex validation

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the NewBooking window which is opened from the MainWindow. 
    *  
    *  It allows the user to enter details of a new Booking (including the guests & extras). The details are then saved to the SQLite database. 
    *  
    *  Last Updated 30/11/16 23:24
    */

    public partial class NewBooking : Window
    {
        public NewBooking()
        {
            InitializeComponent();
            fill_CustCombo();
            fill_DriverCombo();
        }

        private void arrivalPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates how many days the booking is for
        {
            if (arrivalPicker.SelectedDate.HasValue == true && departPicker.SelectedDate.HasValue == true)
            {
                DateTime sDate = arrivalPicker.SelectedDate.Value; // Gets departure date value 
                DateTime eDate = departPicker.SelectedDate.Value; // Gets departure date value
                TimeSpan ts = eDate - sDate; // Calculates the difference in days
                int differenceInDays = ts.Days;
                nightsNum.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
            }
        }

        private void departPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates how many days the booking is for
        {
            if (arrivalPicker.SelectedDate.HasValue == true && departPicker.SelectedDate.HasValue == true)
            {
                DateTime sDate = arrivalPicker.SelectedDate.Value; // Gets departure date value 
                DateTime eDate = departPicker.SelectedDate.Value; // Gets departure date value
                TimeSpan ts = eDate - sDate; // Calculates the difference in days
                int differenceInDays = ts.Days;
                nightsNum.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
            }
        }

        private void startPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates the number of days the care will be hired for
        {
            if (startPicker.SelectedDate.HasValue == true && endPicker.SelectedDate.HasValue == true)
            {
                DateTime sDate = startPicker.SelectedDate.Value; // Gets departure date value 
                DateTime eDate = endPicker.SelectedDate.Value; // Gets departure date value
                TimeSpan ts = eDate - sDate; // Calculates the difference in days
                int differenceInDays = ts.Days;
                hireDays.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
            }
        }

        private void endPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates the number of days the care will be hired for
        {
            if (startPicker.SelectedDate.HasValue == true && endPicker.SelectedDate.HasValue == true)
            {
                DateTime sDate = startPicker.SelectedDate.Value; // Gets departure date value 
                DateTime eDate = endPicker.SelectedDate.Value; // Gets departure date value
                TimeSpan ts = eDate - sDate; // Calculates the difference in days
                int differenceInDays = ts.Days;
                hireDays.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
            }
        }

        private void hireBox_Checked(object sender, RoutedEventArgs e) // If the car hire checkbox is checked, the booking dates will be transferred to the car hire dates. It is assumed the car hire will be for the enitre holiday unless the users changes the car hire dates manually.
        {
            startPicker.SelectedDate = arrivalPicker.SelectedDate;
            endPicker.SelectedDate = departPicker.SelectedDate;
        }

        public void saveBtn_Click(object sender, RoutedEventArgs e) // When the Save button is clicked, the booking details will be added to the database
        {
            // Regex validation objects used to validate Guest details
            Regex namePattern = new Regex(@"^[a-zA-Z\s]*\z$"); // Name entered must be alphabetical characters only (a-z & A-Z)
            Regex agePattern = new Regex(@"^[0-9]{1,2}\z$"); // Age entered must be numeric characters only (0-9) and may only be two charaters long
            Regex passportPattern = new Regex(@"^[0-9a-zA-Z]{10}\z$"); // Passport number entered must be in Alpha/Numeric format and must be 10 characters long

            // Create new Singleton Instances
            Booking newBooking = Booking.Instance;
            Guest addGuest = Guest.Instance;
            Extras extras = Extras.Instance;

            // Set the Booking properties
            newBooking.setBookingRef(); // // Sets auto increment Booking reference number
            newBooking.CustName = custCombo.Text; 
            newBooking.findCustRef(); // Method called to find the customer reference
            newBooking.arrivalDate = arrivalPicker.SelectedDate.Value; 
            newBooking.departureDate = departPicker.SelectedDate.Value; 
            newBooking.Nights = Convert.ToInt32(nightsNum.Text); 

            // Guest 1
            if (name1Box.Text != String.Empty && age1Box.Text != String.Empty && passport1Box.Text != String.Empty) // If textboxes ARE NOT empty - Validate first - Then set values
            {
                if (namePattern.IsMatch(name1Box.Text) == false)
                {
                    MessageBox.Show("Guest 1's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                if (agePattern.IsMatch(age1Box.Text) == false)
                {
                    MessageBox.Show("Guest 1's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                if (passportPattern.IsMatch(passport1Box.Text) == false)
                {
                    MessageBox.Show("Guest 1's passport number is not valid. Passport must be in Alpha/Numeric format and must be 10 characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                addGuest.guest1Name = name1Box.Text;
                addGuest.guest1Age = Convert.ToInt32(age1Box.Text);
                addGuest.guest1PassNum = passport1Box.Text;
            }
            else // If text boxes were left blank or partially blank
            {
                addGuest.guest1Name = "Not applicable";
                addGuest.guest1Age = 0;
                addGuest.guest1PassNum = "0000000000";
            }

            // Guest 2
            if (name2Box.Text != String.Empty && age2Box.Text != String.Empty && passport2Box.Text != String.Empty) // If textboxes ARE NOT empty - Validate first - Then set values
            {
                if (namePattern.IsMatch(name2Box.Text) == false)
                {
                    MessageBox.Show("Guest 2's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                if (agePattern.IsMatch(age2Box.Text) == false)
                {
                    MessageBox.Show("Guest 2's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                if (passportPattern.IsMatch(passport2Box.Text) == false)
                {
                    MessageBox.Show("Guest 2's passport number is not valid. Passport must be in Alpha/Numeric format and must be 10 characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                addGuest.guest2Name = name2Box.Text;
                addGuest.guest2Age = Convert.ToInt32(age2Box.Text);
                addGuest.guest2PassNum = passport2Box.Text;
            }
            else // If text boxes were left blank or partially blank
            {
                addGuest.guest2Name = "Not applicable";
                addGuest.guest2Age = 0;
                addGuest.guest2PassNum = "0000000000";
            }

            // Guest 3
            if (name3Box.Text != String.Empty && age3Box.Text != String.Empty && passport3Box.Text != String.Empty) // If textboxes ARE NOT empty - Validate first - Then set values
            {
                if (namePattern.IsMatch(name3Box.Text) == false)
                {
                    MessageBox.Show("Guest 3's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                if (agePattern.IsMatch(age3Box.Text) == false)
                {
                    MessageBox.Show("Guest 3's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                if (passportPattern.IsMatch(passport3Box.Text) == false)
                {
                    MessageBox.Show("Guest 3's passport number is not valid. Passport must be in Alpha/Numeric format and must be 10 characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                addGuest.guest3Name = name3Box.Text;
                addGuest.guest3Age = Convert.ToInt32(age3Box.Text);
                addGuest.guest3PassNum = passport3Box.Text;
            }
            else // If text boxes were left blank or partially blank
            {
                addGuest.guest3Name = "Not applicable";
                addGuest.guest3Age = 0;
                addGuest.guest3PassNum = "0000000000";
            }

            // Guest 4
            if (name4Box.Text != String.Empty && age4Box.Text != String.Empty && passport4Box.Text != String.Empty) // If textboxes ARE NOT empty - Validate first - Then set values
            {
                if (namePattern.IsMatch(name4Box.Text) == false)
                {
                    MessageBox.Show("Guest 4's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                if (agePattern.IsMatch(age4Box.Text) == false)
                {
                    MessageBox.Show("Guest 4's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                if (passportPattern.IsMatch(passport4Box.Text) == false)
                {
                    MessageBox.Show("Guest 4's passport number is not valid. Passport must be in Alpha/Numeric format and must be 10 characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                    return;
                }
                addGuest.guest4Name = name4Box.Text;
                addGuest.guest4Age = Convert.ToInt32(age4Box.Text);
                addGuest.guest4PassNum = passport4Box.Text;
            }
            else // If text boxes were left blank or partially blank
            {
                addGuest.guest4Name = "Not applicable";
                addGuest.guest4Age = 0;
                addGuest.guest4PassNum = "0000000000";
            }

            // Extras information
            if (breakfastBox.IsChecked.HasValue && breakfastBox.IsChecked.Value) // Breakfast Meals - Set properties if checked
            {
                extras.breakfastMeals = "Yes";
                extras.dietRequirements = dietBox.Text;
            }
            else
            {
                extras.breakfastMeals = "No";
            }

            if (eveningBox.IsChecked.HasValue && eveningBox.IsChecked.Value) // Evening Meals - Set properties if checked
            {
                extras.eveningMeals = "Yes";
                extras.dietRequirements = dietBox.Text;
            }
            else
            {
                extras.eveningMeals = "No";
            }

            if (hireBox.IsChecked.HasValue && hireBox.IsChecked.Value) // Car hire - Set properties if checked
            {
                extras.carHire = "Yes";
                extras.hireStart = startPicker.SelectedDate.Value;
                extras.hireEnd = endPicker.SelectedDate.Value;
                extras.driverName = driverCombo.Text;
                extras.hireDays = Convert.ToInt32(hireDays.Text);
            }
            else
            {
                extras.carHire = "No";
                extras.driverName = "N/A";
                extras.hireDays = 0;
            }

            // Add details to the database
            newBooking.AddBooking(); // Adds the booking to the database (See Booking class)
            addGuest.BookingRef = newBooking.BookingRef;
            extras.BookingRef = newBooking.BookingRef;

            addGuest.addGuests(); // Method to add guest to database (See Guest class)
            extras.addExtras(); // Method to add extras to database (See Extras class)

            MessageBox.Show("Booking details saved"); // Message box shown to user
            this.Close(); // Closes the window after booking details have been saved
        }

        void fill_CustCombo() // This method will fill the custCombo with Customer names from the database. This will identify which customer the booking is for.  
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Customer"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string Name = reader.GetString(1); // Column 1 indicates the Customer Name column in the SQLite database
                custCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        void fill_DriverCombo() // This method will fill the driverCombo with Driver names from the database.
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Drivers"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string Name = reader.GetString(0); // Column 1 indicates the Customer Name column in the SQLite database
                driverCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }
    }
}
