﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This GUI (Invoice) will show the customers invoice
    *  
    *  Last Updated 30/11/16 23:24
    */

    public partial class Invoice : Window
    {
        public Invoice()
        {
            InitializeComponent();
            fill_CustCombo();
        }

        string _custName; // Local variable used for findCustRef method
        int _custReference; // Local variable used for finding the booking details from the database
        int _bookRef; // Local variable used for storing the booking reference number
        string sArrival; // Local variable used for storing the arrival date in string format

        Customer invoiceCustomer = Customer.Instance;
        Booking invoiceBooking = Booking.Instance;
        Guest invoiceGuest = Guest.Instance;
        Extras invoiceExtras = Extras.Instance;

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command; // creates new database command

        // WPF events start here
        private void custCombo_DropDownClosed(object sender, EventArgs e) // After the user selects a customer to amend, the booking combo box will populate with bookings they have made
        {
            if (custCombo.Text == String.Empty) // If the combobox is closed but missing string, show message
            {
                MessageBox.Show("Please select a customer from the dropdown box");
            }
            else // Retrieve the customer details from the database and fill in the boxes
            {
                _custName = custCombo.Text;
                findCustRef(); // Find the customer reference in the database
                bookingCombo.Items.Clear();
                fill_BookingCombo(); // Fill the booking combobox with the bookings the customer has made
            }
        }

        private void custCombo_SelectionChanged(object sender, SelectionChangedEventArgs e) // If the user selects the wrong customer and chooses another, the event will automatically update the booking combo
        {
            bookingCombo.Text = String.Empty;
            bookingCombo.Items.Clear();
            _custName = custCombo.Text;
            findCustRef(); // Find the customer reference in the database
            fill_BookingCombo(); // Fill the booking combobox with the bookings the customer has made
        }

        private void bookingCombo_DropDownClosed(object sender, EventArgs e) // When the dropdown is closed, the fields shown on the window should populate with data of the Booking, Guest & Extras
        {
            if (bookingCombo.Text == String.Empty) // If the combobox is closed but missing string, show message
            {
                MessageBox.Show("Please select a booking from the dropdown box");
            }
            else
            {
                // Fill in booking details
                sArrival = bookingCombo.Text;
                getBookingDetails(); // Get the booking details from the database and fill in the boxes
                bookingref.Text = Convert.ToString(invoiceBooking.BookingRef);
                custName.Text = invoiceCustomer.Name;
                custAge.Text = Convert.ToString(invoiceCustomer.Age);
                arrivial.Text = invoiceBooking.arrivalDate.ToShortDateString();
                departure.Text = invoiceBooking.departureDate.ToShortDateString();
                nightsNum.Text = Convert.ToString(invoiceBooking.Nights);

                // Fill in guest details
                invoiceGuest.resetGuests();
                getGuestDetails();
                invoiceGuest.countGuests();
                numGuests.Text = Convert.ToString(invoiceGuest.totalGuests);
                invoiceGuest.countTeenGuests();
                teenGuests.Text = Convert.ToString(invoiceGuest.totalTeenGuests);

                // Fill in extra details
                invoiceExtras.resetExtras();
                getExtrasDetails();
                breakfastsBlock.Text = invoiceExtras.breakfastMeals;
                eveningsBlock.Text = invoiceExtras.eveningMeals;
                hireBlock.Text = invoiceExtras.carHire;
                hireDaysBlock.Text = Convert.ToString(invoiceExtras.hireDays);

                // Calculate Booking charges
                nightsNum1.Text = Convert.ToString(invoiceBooking.Nights);
                if(invoiceCustomer.Age < 18)
                {
                    bookingCost.Text = "30";
                }
                if (invoiceCustomer.Age >= 18)
                {
                    bookingCost.Text = "50";
                }
                totalBookingCost.Text = Convert.ToString((invoiceBooking.Nights * Convert.ToInt32(bookingCost.Text)));

                // Calculate Guest charges
                if (invoiceGuest.totalGuests >= 1)
                {
                    numOfGuests.Text = Convert.ToString(invoiceGuest.totalGuests);
                    nightsNum2.Text = Convert.ToString(invoiceBooking.Nights);
                    guestCost.Text = "50";
                    totalGuestCost.Text = Convert.ToString(invoiceGuest.totalGuests * (invoiceBooking.Nights * 50));
                }
                if (invoiceGuest.totalTeenGuests >= 1)
                {
                    numOfTeenGuests.Text = Convert.ToString(invoiceGuest.totalTeenGuests);
                    nightsNum3.Text = Convert.ToString(invoiceBooking.Nights);
                    teenGuestCost.Text = "30";
                    totalTeenGuestCost.Text = Convert.ToString(invoiceGuest.totalTeenGuests * (invoiceBooking.Nights * 30));
                }

                // Calculate Extra charges
                if (invoiceExtras.breakfastMeals == "Yes")
                {
                    numOfPeople.Text = Convert.ToString(1 + invoiceGuest.totalGuests + invoiceGuest.totalTeenGuests);
                    nightsNum4.Text = Convert.ToString(invoiceBooking.Nights);
                    breakfastCost.Text = "5";
                    totalBreakfastCost.Text = Convert.ToString((1 + invoiceGuest.totalGuests + invoiceGuest.totalTeenGuests) * (invoiceBooking.Nights * 5));
                }
                if (invoiceExtras.eveningMeals == "Yes")
                {
                    numOfPeople2.Text = Convert.ToString(1 + invoiceGuest.totalGuests + invoiceGuest.totalTeenGuests);
                    nightsNum5.Text = Convert.ToString(invoiceBooking.Nights);
                    eveningCost.Text = "15";
                    totalEveningCost.Text = Convert.ToString((1 + invoiceGuest.totalGuests + invoiceGuest.totalTeenGuests) * (invoiceBooking.Nights * 15));
                }
                if (invoiceExtras.carHire == "Yes")
                {
                    numHireDays.Text = Convert.ToString(invoiceExtras.hireDays);
                    hireCost.Text = "50";
                    totalHireCost.Text = Convert.ToString(invoiceExtras.hireDays * 50);
                }

                // Calculate the final cost
                finalBookingCost.Text = Convert.ToString(
                Convert.ToInt32(totalBookingCost.Text)+
                Convert.ToInt32(totalGuestCost.Text)+
                Convert.ToInt32(totalTeenGuestCost.Text)+
                Convert.ToInt32(totalBreakfastCost.Text)+
                Convert.ToInt32(totalEveningCost.Text)+
                Convert.ToInt32(totalHireCost.Text));
            }
        }

        private void bookingCombo_SelectionChanged(object sender, SelectionChangedEventArgs e) // If the customer selects the wrong booking and chooses another
        {
            clearWindow(); // Clear the textboxes, checkboxes and datepickers of all values
        }

        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        // Methods start here
        private void fill_CustCombo() // This method will fill the custCombo with Customer names from the database. This will identify which customer the booking is for.  
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Customer"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string Name = reader.GetString(1);
                custCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        private void fill_BookingCombo() // This method will fill the booking combo box with arrival dates. This is handy if the customer has more than one booking.
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Booking WHERE CustRef = '" + this._custReference + "';"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                sArrival = reader.GetString(2);
                bookingCombo.Items.Add(sArrival); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        private void findCustRef() // Method to find the customer reference in the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Customer WHERE Name = '" + this._custName + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                this._custReference = reader.GetInt32(0);
                invoiceCustomer.Name = reader.GetString(1);
                invoiceCustomer.Age = reader.GetInt32(2);
            }
            connection.Close(); // Close the database connection
        }

        private void getBookingDetails() // Method to get booking details from the database 
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Booking WHERE CustRef = '" + this._custReference + "' AND ArrivalDate = '" + this.sArrival + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                this._bookRef = reader.GetInt32(0);
                invoiceBooking.BookingRef = this._bookRef;
                invoiceBooking.CustRef = reader.GetInt32(1);
                string arrival = reader.GetString(2);
                invoiceBooking.arrivalDate = DateTime.Parse(arrival);
                string depart = reader.GetString(3);
                invoiceBooking.departureDate = DateTime.Parse(depart);
                invoiceBooking.Nights = reader.GetInt32(4);
            }
            connection.Close(); // Close the database connection
        }

        private void getGuestDetails() // Method for getting Guest data from the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Guests WHERE BookingRef = '" + this._bookRef + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                invoiceGuest.BookingRef = this._bookRef;
                // Guest 1
                invoiceGuest.guest1Name = reader.GetString(1);
                invoiceGuest.guest1Age = reader.GetInt32(2);
                invoiceGuest.guest1PassNum = reader.GetString(3);
                // Guest 2
                invoiceGuest.guest2Name = reader.GetString(4);
                invoiceGuest.guest2Age = reader.GetInt32(5);
                invoiceGuest.guest2PassNum = reader.GetString(6);
                // Guest 3
                invoiceGuest.guest3Name = reader.GetString(7);
                invoiceGuest.guest3Age = reader.GetInt32(8);
                invoiceGuest.guest3PassNum = reader.GetString(9);
                // Guest 4
                invoiceGuest.guest4Name = reader.GetString(10);
                invoiceGuest.guest4Age = reader.GetInt32(11);
                invoiceGuest.guest4PassNum = reader.GetString(12);
            }
            connection.Close(); // Close the database connection
        }

        private void getExtrasDetails() // Method for getting Extras data from the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Extras WHERE BookingRef = '" + this._bookRef + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                invoiceExtras.BookingRef = this._bookRef;
                invoiceExtras.breakfastMeals = reader.GetString(1);
                invoiceExtras.eveningMeals = reader.GetString(2);
                invoiceExtras.dietRequirements = reader.GetString(3);
                invoiceExtras.carHire = reader.GetString(4);
                string _hireStart = reader.GetString(5);
                invoiceExtras.hireStart = DateTime.Parse(_hireStart);
                string _hireEnd = reader.GetString(6);
                invoiceExtras.hireEnd = DateTime.Parse(_hireEnd);
                invoiceExtras.driverName = reader.GetString(7);
                invoiceExtras.hireDays = reader.GetInt32(8);
            }
            connection.Close(); // Close the database connection
        }

        private void clearWindow() // Method to everything from the window
        {
            bookingref.Text = "?";
            custName.Text = "?";
            custAge.Text = "?";
            arrivial.Text = "?";
            departure.Text = "?";
            nightsNum.Text = "?";
            numGuests.Text = "?";
            teenGuests.Text = "?";
            breakfastsBlock.Text = "?";
            eveningsBlock.Text = "?";
            hireBlock.Text = "?";
            hireDaysBlock.Text = "?";
            nightsNum1.Text = "?";
            nightsNum2.Text = "0";
            nightsNum3.Text = "0";
            bookingCost.Text = "0";
            totalBookingCost.Text = "0";
            numOfGuests.Text = "0";
            guestCost.Text = "0";
            totalGuestCost.Text = "0";
            numOfTeenGuests.Text = "0";
            teenGuestCost.Text = "0";
            totalTeenGuestCost.Text = "0";
            numOfPeople.Text = "0";
            nightsNum4.Text = "0";
            breakfastCost.Text = "0";
            totalBreakfastCost.Text = "0";
            numOfPeople2.Text = "0";
            nightsNum5.Text = "0";
            eveningCost.Text = "0";
            totalEveningCost.Text = "0";
            numHireDays.Text = "0";
            hireCost.Text = "0";
            totalHireCost.Text = "0";
            finalBookingCost.Text = "0";
        }
    }
}
