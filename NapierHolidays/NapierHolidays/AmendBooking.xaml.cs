﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite; // For SQLite database
using System.Text.RegularExpressions; // For Regex validation

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the AmendBooking window which is opened from the MainWindow. 
    *  
    *  It allows the user to Amend or Delete a Booking
    *  
    *  Last Updated 30/11/16 23:24
    */

    public partial class AmendBooking : Window
    {
        public AmendBooking()
        {
            InitializeComponent();
            fill_CustCombo();
            fill_DriverCombo();
        }

        string _custName; // Local variable used for findCustRef method
        int _custReference; // Local variable used for finding the booking details from the database
        int _bookRef; // Local variable used for storing the booking reference number
        string sArrival; // Local variable used for storing the arrival date in string format

        Booking amendBooking = Booking.Instance;
        Guest amendGuest = Guest.Instance;
        Extras amendExtras = Extras.Instance;

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command; // creates new database command

        // WPF events start here
        private void arrivalPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates how many days the booking is for
        {
            if (arrivalPicker.SelectedDate.HasValue == true && departPicker.SelectedDate.HasValue == true)
            {
                DateTime sDate = arrivalPicker.SelectedDate.Value; // Gets departure date value 
                DateTime eDate = departPicker.SelectedDate.Value; // Gets departure date value
                TimeSpan ts = eDate - sDate; // Calculates the difference in days
                int differenceInDays = ts.Days;
                nightsNum.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
            }
        }

        private void departPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates how many days the booking is for
        {
            if (arrivalPicker.SelectedDate.HasValue == true && departPicker.SelectedDate.HasValue == true)
            {
                DateTime sDate = arrivalPicker.SelectedDate.Value; // Gets departure date value 
                DateTime eDate = departPicker.SelectedDate.Value; // Gets departure date value
                TimeSpan ts = eDate - sDate; // Calculates the difference in days
                int differenceInDays = ts.Days;
                nightsNum.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
            }
        }

        private void startPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates the number of days the care will be hired for
        {
            if (startPicker.SelectedDate.HasValue == true && endPicker.SelectedDate.HasValue == true)
            {
                DateTime sDate = startPicker.SelectedDate.Value; // Gets departure date value 
                DateTime eDate = endPicker.SelectedDate.Value; // Gets departure date value
                TimeSpan ts = eDate - sDate; // Calculates the difference in days
                int differenceInDays = ts.Days;
                hireDays.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
            }
        }

        private void endPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates the number of days the care will be hired for
        {
            if (startPicker.SelectedDate.HasValue == true && endPicker.SelectedDate.HasValue == true)
            {
                DateTime sDate = startPicker.SelectedDate.Value; // Gets departure date value 
                DateTime eDate = endPicker.SelectedDate.Value; // Gets departure date value
                TimeSpan ts = eDate - sDate; // Calculates the difference in days
                int differenceInDays = ts.Days;
                hireDays.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
            }
        }

        private void custCombo_DropDownClosed(object sender, EventArgs e) // After the user selects a customer to amend, the booking combo box will populate with bookings they have made
        {
            if (custCombo.Text == String.Empty) // If the combobox is closed but missing string, show message
            {
                MessageBox.Show("Please select a customer from the dropdown box");
            }
            else // Retrieve the customer details from the database and fill in the boxes
            {
                _custName = custCombo.Text;
                findCustRef(); // Find the customer reference in the database
                bookingCombo.Items.Clear();
                fill_BookingCombo(); // Fill the booking combobox with the bookings the customer has made
            }
        }

        private void custCombo_SelectionChanged(object sender, SelectionChangedEventArgs e) // If the user selects the wrong customer and chooses another, the event will automatically update the booking combo
        {
            bookingCombo.Text = String.Empty;
            bookingCombo.Items.Clear();
            _custName = custCombo.Text;
            findCustRef(); // Find the customer reference in the database
            fill_BookingCombo(); // Fill the booking combobox with the bookings the customer has made
        }

        private void bookingCombo_DropDownClosed(object sender, EventArgs e) // When the dropdown is closed, the fields shown on the window should populate with data of the Booking, Guest & Extras
        {
            if (bookingCombo.Text == String.Empty) // If the combobox is closed but missing string, show message
            {
                MessageBox.Show("Please select a booking from the dropdown box");
            }
            else
            {
                // Fill in booking details
                sArrival = bookingCombo.Text;
                getBookingDetails(); // Get the booking details from the database and fill in the boxes
                arrivalPicker.SelectedDate = amendBooking.arrivalDate;
                departPicker.SelectedDate = amendBooking.departureDate;
                nightsNum.Text = Convert.ToString(amendBooking.Nights);

                // Fill in guest details
                amendGuest.resetGuests(); // Reset the Guest properties first
                getGuestDetails(); // Get Guest details from the database and fill in the boxes
                name1Box.Text = amendGuest.guest1Name;
                age1Box.Text = Convert.ToString(amendGuest.guest1Age);
                passport1Box.Text = Convert.ToString(amendGuest.guest1PassNum);
                name2Box.Text = amendGuest.guest2Name;
                age2Box.Text = Convert.ToString(amendGuest.guest2Age);
                passport2Box.Text = Convert.ToString(amendGuest.guest2PassNum);
                name3Box.Text = amendGuest.guest3Name;
                age3Box.Text = Convert.ToString(amendGuest.guest3Age);
                passport3Box.Text = Convert.ToString(amendGuest.guest3PassNum);
                name4Box.Text = amendGuest.guest4Name;
                age4Box.Text = Convert.ToString(amendGuest.guest4Age);
                passport4Box.Text = Convert.ToString(amendGuest.guest4PassNum);

                // Fill in Extras details
                amendExtras.resetExtras(); // Reset the Extras properties first
                getExtrasDetails(); // Get Extras details from the database and fill in the boxes
                if (amendExtras.breakfastMeals == "Yes")
                {
                    breakfastBox.SetCurrentValue(CheckBox.IsCheckedProperty, true);
                }
                if (amendExtras.eveningMeals == "Yes")
                {
                    eveningBox.SetCurrentValue(CheckBox.IsCheckedProperty, true);
                }
                dietBox.Text = amendExtras.dietRequirements;
                if (amendExtras.carHire == "Yes")
                {
                    hireBox.SetCurrentValue(CheckBox.IsCheckedProperty, true);
                    startPicker.SelectedDate = amendExtras.hireStart;
                    endPicker.SelectedDate = amendExtras.hireEnd;
                    driverCombo.Text = amendExtras.driverName;
                    hireDays.Text = Convert.ToString(amendExtras.hireDays);
                }
            }
        }

        private void bookingCombo_SelectionChanged(object sender, SelectionChangedEventArgs e) // If the customer selects the wrong booking and chooses another
        {
            clearWindow(); // Clear the textboxes, checkboxes and datepickers of all values
        }

        private void hireBox_Checked(object sender, RoutedEventArgs e) // If the car hire checkbox is checked, the booking dates will be transferred to the car hire dates. It is assumed the car hire will be for the enitre holiday unless the users changes the car hire dates manually. 
        {
            startPicker.SelectedDate = arrivalPicker.SelectedDate;
            endPicker.SelectedDate = departPicker.SelectedDate;
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e) // When clicked, the Booking, Guests & Extras data held in the database will be updated with whatever the user has entered in this window. 
        {
            // Regex validation objects used to validate Guest details
            Regex namePattern = new Regex(@"^[a-zA-Z\s]*\z$"); // Name entered must be alphabetical characters only (a-z & A-Z)
            Regex agePattern = new Regex(@"^[0-9]{1,2}\z$"); // Age entered must be numeric characters only (0-9) and may only be two charaters long
            Regex passportPattern = new Regex(@"^[0-9a-zA-Z]{10}\z$"); // Passport number entered must be in Alpha/Numeric format and must be 10 characters long

            // Create new Singleton Instances
            Booking updateBooking = Booking.Instance;
            Guest updateGuest = Guest.Instance;
            Extras updateExtras = Extras.Instance;

            if (bookingCombo.Text != String.Empty) // If a booking has been selected from the combo box - do the following
            {
                // Set the Booking properties
                updateBooking.CustName = custCombo.Text;
                updateBooking.findCustRef(); // Method called to find the customer reference
                updateBooking.arrivalDate = arrivalPicker.SelectedDate.Value;
                updateBooking.departureDate = departPicker.SelectedDate.Value;
                updateBooking.Nights = Convert.ToInt32(nightsNum.Text);

                updateBooking.updateBooking(); // Adds the booking details to the database
                updateBooking.findBookingRef(); // Gets the booking reference

                // Guest 1
                if (name1Box.Text != String.Empty && age1Box.Text != String.Empty && passport1Box.Text != String.Empty) // If textboxes ARE NOT empty - Validate first - Then set values
                {
                    if (namePattern.IsMatch(name1Box.Text) == false)
                    {
                        MessageBox.Show("Guest 1's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    if (agePattern.IsMatch(age1Box.Text) == false)
                    {
                        MessageBox.Show("Guest 1's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    if (passportPattern.IsMatch(passport1Box.Text) == false)
                    {
                        MessageBox.Show("Guest 1's passport number is not valid. Passport must be in Alpha/Numeric format and must be 10 characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    updateGuest.guest1Name = name1Box.Text;
                    updateGuest.guest1Age = Convert.ToInt32(age1Box.Text);
                    updateGuest.guest1PassNum = passport1Box.Text;
                }
                else // If text boxes were left blank or partially blank
                {
                    updateGuest.guest1Name = "Not applicable";
                    updateGuest.guest1Age = 0;
                    updateGuest.guest1PassNum = "0";
                }

                // Guest 2
                if (name2Box.Text != String.Empty && age2Box.Text != String.Empty && passport2Box.Text != String.Empty) // If textboxes ARE NOT empty - Validate first - Then set values
                {
                    if (namePattern.IsMatch(name2Box.Text) == false)
                    {
                        MessageBox.Show("Guest 2's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    if (agePattern.IsMatch(age2Box.Text) == false)
                    {
                        MessageBox.Show("Guest 2's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    if (passportPattern.IsMatch(passport2Box.Text) == false)
                    {
                        MessageBox.Show("Guest 2's passport number is not valid. Passport must be in Alpha/Numeric format and must be 10 characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    updateGuest.guest2Name = name2Box.Text;
                    updateGuest.guest2Age = Convert.ToInt32(age2Box.Text);
                    updateGuest.guest2PassNum = passport2Box.Text;
                }
                else // If text boxes were left blank or partially blank
                {
                    updateGuest.guest2Name = "Not applicable";
                    updateGuest.guest2Age = 0;
                    updateGuest.guest2PassNum = "0";
                }

                // Guest 3
                if (name3Box.Text != String.Empty && age3Box.Text != String.Empty && passport3Box.Text != String.Empty) // If textboxes ARE NOT empty - Validate first - Then set values
                {
                    if (namePattern.IsMatch(name3Box.Text) == false)
                    {
                        MessageBox.Show("Guest 3's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    if (agePattern.IsMatch(age3Box.Text) == false)
                    {
                        MessageBox.Show("Guest 3's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    if (passportPattern.IsMatch(passport3Box.Text) == false)
                    {
                        MessageBox.Show("Guest 3's passport number is not valid. Passport must be in Alpha/Numeric format and must be 10 characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    updateGuest.guest3Name = name3Box.Text;
                    updateGuest.guest3Age = Convert.ToInt32(age3Box.Text);
                    updateGuest.guest3PassNum = passport3Box.Text;
                }
                else // If text boxes were left blank or partially blank
                {
                    updateGuest.guest3Name = "Not applicable";
                    updateGuest.guest3Age = 0;
                    updateGuest.guest3PassNum = "0";
                }

                // Guest 4
                if (name4Box.Text != String.Empty && age4Box.Text != String.Empty && passport4Box.Text != String.Empty) // If textboxes ARE NOT empty - Validate first - Then set values
                {
                    if (namePattern.IsMatch(name4Box.Text) == false)
                    {
                        MessageBox.Show("Guest 4's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    if (agePattern.IsMatch(age4Box.Text) == false)
                    {
                        MessageBox.Show("Guest 4's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    if (passportPattern.IsMatch(passport4Box.Text) == false)
                    {
                        MessageBox.Show("Guest 4's passport number is not valid. Passport must be in Alpha/Numeric format and must be 10 characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                        return;
                    }
                    updateGuest.guest4Name = name4Box.Text;
                    updateGuest.guest4Age = Convert.ToInt32(age4Box.Text);
                    updateGuest.guest4PassNum = passport4Box.Text;
                }
                else // If text boxes were left blank or partially blank
                {
                    updateGuest.guest4Name = "Not applicable";
                    updateGuest.guest4Age = 0;
                    updateGuest.guest4PassNum = "0";
                }

                // Extras information
                if (breakfastBox.IsChecked.HasValue && breakfastBox.IsChecked.Value) // Breakfast Meals - Set properties if checked
                {
                    updateExtras.breakfastMeals = "Yes";
                    updateExtras.dietRequirements = dietBox.Text;
                }
                else
                {
                    updateExtras.breakfastMeals = "No";
                }

                if (eveningBox.IsChecked.HasValue && eveningBox.IsChecked.Value) // Evening Meals - Set properties if checked
                {
                    updateExtras.eveningMeals = "Yes";
                    updateExtras.dietRequirements = dietBox.Text;
                }
                else
                {
                    updateExtras.eveningMeals = "No";
                }

                if (hireBox.IsChecked.HasValue && hireBox.IsChecked.Value) // Car hire - Set properties if checked
                {
                    updateExtras.carHire = "Yes";
                    updateExtras.hireStart = startPicker.SelectedDate.Value;
                    updateExtras.hireEnd = endPicker.SelectedDate.Value;
                    updateExtras.driverName = driverCombo.Text;
                    updateExtras.hireDays = Convert.ToInt32(hireDays.Text);
                }
                else
                {
                    updateExtras.carHire = "No";
                    updateExtras.driverName = "N/A";
                    updateExtras.hireDays = 0;
                }

                // Add details to the database
                updateBooking.updateBooking(); // Adds the booking to the database (See Booking class)
                updateBooking.findBookingRef(); // Method called to find the booking reference
                updateGuest.BookingRef = updateBooking.BookingRef;
                updateExtras.BookingRef = updateBooking.BookingRef;

                updateGuest.updateGuests(); // Method to add guest to database (See Guest class)
                updateExtras.updateExtras(); // Method to add extras to database (See Extras class)

                MessageBox.Show("Booking details updated"); // Message box shown to user
                this.Close(); // Closes the window after booking details have been saved
            }
            else
            {
                MessageBox.Show("You have not selected a booking to update");
            }
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e) // When clicked, Extras, Guest then Booking data will be deleted from the data base
        {
            if (bookingCombo.Text != String.Empty)
            {
                amendExtras.deleteExtras();
                amendGuest.deleteGuests();
                amendBooking.deleteBooking();
                MessageBox.Show("Booking details deleted"); // Message box shown to user
                this.Close(); // Closes the window after booking details have been saved
            }
            else
            {
                MessageBox.Show("You have not selected a booking to delete");
            }
        }

        // Methods start here
        private void fill_CustCombo() // This method will fill the custCombo with Customer names from the database. This will identify which customer the booking is for.  
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Customer"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string Name = reader.GetString(1); 
                custCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        private void fill_BookingCombo() // This method will fill the booking combo box with arrival dates. This is handy if the customer has more than one booking.
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Booking WHERE CustRef = '" + this._custReference + "';"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                sArrival = reader.GetString(2);
                bookingCombo.Items.Add(sArrival); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        void fill_DriverCombo() // This method will fill the driverCombo with Driver names from the database.
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Drivers"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string Name = reader.GetString(0); // Column 1 indicates the Customer Name column in the SQLite database
                driverCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        private void findCustRef() // Method to find the customer reference in the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT CustRef FROM Customer WHERE Name = '" + this._custName + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                this._custReference = reader.GetInt32(0); 
            }
            connection.Close(); // Close the database connection
        }

        private void getBookingDetails() // Method to get booking details from the database 
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Booking WHERE CustRef = '" + this._custReference + "' AND ArrivalDate = '" + this.sArrival + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                this._bookRef = reader.GetInt32(0);
                amendBooking.BookingRef = this._bookRef;
                amendBooking.CustRef = reader.GetInt32(1);
                string arrival = reader.GetString(2);
                amendBooking.arrivalDate = DateTime.Parse(arrival);
                string depart = reader.GetString(3);
                amendBooking.departureDate = DateTime.Parse(depart);
                amendBooking.Nights = reader.GetInt32(4);
            }
            connection.Close(); // Close the database connection
        }

        private void getGuestDetails() // Method for getting Guest data from the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Guests WHERE BookingRef = '" + this._bookRef + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                amendGuest.BookingRef = this._bookRef;
                // Guest 1
                amendGuest.guest1Name = reader.GetString(1);
                amendGuest.guest1Age = reader.GetInt32(2);
                amendGuest.guest1PassNum = reader.GetString(3);
                // Guest 2
                amendGuest.guest2Name = reader.GetString(4);
                amendGuest.guest2Age = reader.GetInt32(5);
                amendGuest.guest2PassNum = reader.GetString(6);
                // Guest 3
                amendGuest.guest3Name = reader.GetString(7);
                amendGuest.guest3Age = reader.GetInt32(8);
                amendGuest.guest3PassNum = reader.GetString(9);               
                // Guest 4
                amendGuest.guest4Name = reader.GetString(10);
                amendGuest.guest4Age = reader.GetInt32(11);
                amendGuest.guest4PassNum = reader.GetString(12);
            }
            connection.Close(); // Close the database connection
        }

        private void getExtrasDetails() // Method for getting Extras data from the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Extras WHERE BookingRef = '" + this._bookRef + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                amendExtras.BookingRef = this._bookRef;
                amendExtras.breakfastMeals = reader.GetString(1);
                amendExtras.eveningMeals = reader.GetString(2);
                amendExtras.dietRequirements = reader.GetString(3);
                amendExtras.carHire = reader.GetString(4);
                string _hireStart = reader.GetString(5);
                amendExtras.hireStart = DateTime.Parse(_hireStart);
                string _hireEnd = reader.GetString(6);
                amendExtras.hireEnd = DateTime.Parse(_hireEnd);
                amendExtras.driverName = reader.GetString(7);
                amendExtras.hireDays = reader.GetInt32(8);
            }
            connection.Close(); // Close the database connection
        }

        private void clearWindow() // Method to clear the textboxes, checkboxes and datepickers of all values 
        {
            arrivalPicker.SelectedDate = null;
            departPicker.SelectedDate = null;
            nightsNum.Text = String.Empty;
            name1Box.Text = String.Empty;
            age1Box.Text = String.Empty;
            passport1Box.Text = String.Empty;
            name2Box.Text = String.Empty;
            age2Box.Text = String.Empty;
            passport2Box.Text = String.Empty;
            name3Box.Text = String.Empty;
            age3Box.Text = String.Empty;
            passport3Box.Text = String.Empty;
            name4Box.Text = String.Empty;
            age4Box.Text = String.Empty;
            passport4Box.Text = String.Empty;
            breakfastBox.SetCurrentValue(CheckBox.IsCheckedProperty, false);
            eveningBox.SetCurrentValue(CheckBox.IsCheckedProperty, false);
            dietBox.Text = String.Empty;
            hireBox.SetCurrentValue(CheckBox.IsCheckedProperty, false);
            startPicker.SelectedDate = null;
            endPicker.SelectedDate = null;
            driverCombo.Text = String.Empty;
            hireDays.Text = String.Empty;
        }
    }
}
