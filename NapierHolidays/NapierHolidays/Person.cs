﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the Person class which holds the properties for a person's name & address. 
    *  
    *  Customers will inherit these properties. 
    *  
    *  Last Updated 27/11/16 00:15
    */

    public abstract class Person
    {
        public string Name { get; set; } // auto get set for person's name

        public int Age { get; set; } // auto get set for person's age
    }
}
