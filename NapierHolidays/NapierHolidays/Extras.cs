﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the Extras class which has been implemented using the Singleton Design Pattern. 
     *  
     * It will hold the properties related to the extras which are added onto a booking. 
    *  
    *  Last Updated 30/11/16 23:24
    */

    class Extras
    {
        private static Extras instance;

        private Extras() { }

        public int BookingRef { get; set; } // auto get set for booking reference
        public string breakfastMeals { get; set; } // auto get set for breakfast meals
        public string eveningMeals { get; set; } // auto get set for evening meals
        public string dietRequirements { get; set; } // auto get set for dietary requirments
        public string carHire { get; set; } // auto get set for Car hire
        public DateTime hireStart { get; set; } // auto get set for Care hire start date
        public DateTime hireEnd { get; set; } // auto get set for Care hire end date
        public string driverName { get; set; } // auto get set for Driver name
        public int? hireDays { get; set; } // auto get set for the number to days a car will be hired for

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command; // create a new sql command object

        public static Extras Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Extras();
                }
                return instance;
            }

        }
        public void addExtras() // Method for adding a guest to the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "INSERT INTO Extras (BookingRef, BreakfastMeals, EveningMeals, DietDetails, CarHire, HireStartDate, HireEndDate, Driver, HireDays) VALUES ('"+BookingRef+"', '"+breakfastMeals+"', '"+eveningMeals+"', '"+dietRequirements+"', '"+carHire+"', '"+hireStart.ToShortDateString()+"', '"+hireEnd.ToShortDateString()+"', '"+driverName+"', '"+hireDays+"');"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }

        public void resetExtras()
        {
            breakfastMeals = null;
            eveningMeals = null;
            dietRequirements = null;
            carHire = null;
            driverName = null;
            hireDays = null;
        }

        public void updateExtras() // Method for amending Extras details which are held on the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "UPDATE Extras SET BreakfastMeals = '"+breakfastMeals+"', EveningMeals = '"+eveningMeals+"', DietDetails = '"+dietRequirements+"', CarHire = '"+carHire+"', HireStartDate = '"+hireStart.ToShortDateString() + "', HireEndDate = '"+hireEnd.ToShortDateString() + "', Driver = '"+driverName+"', HireDays = '"+hireDays+"' WHERE BookingRef = '" + BookingRef + "';"; // SQL update query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }

        public void deleteExtras() // Method for deleting the Extras details which are held on the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "DELETE FROM Extras WHERE BookingRef = '" + BookingRef + "';"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }
    }
}
