using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the Customer Class which has been implemented using the Singleton Design Pattern and inherits properties from the Person class. 
    *  
    *  It will hold the properties for the customer's Name & Address
    *  
    *  Last Updated 30/11/16 23:24
    */


    public class Customer : Person // Inherits Name & Age from Person Class
    {
        private static Customer instance;

        private Customer() { }

        public int CustRef = 1;
        public string Address { get; set; } // auto get set for address
        public string OldName { get; set; } // auto get set for old name (this is only used when amending or deleting a customer's details)
        public string DuplicateName { get; set; } // auto get set for duplicate name

        public static Customer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Customer();
                }
                return instance;
            }

        }

        public void checkforDuplicates() // Method for checking if the customer details are already saved. We don't want duplicate customers!
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Customer WHERE Name = '" + Name + "';"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string cName = reader.GetString(1); // Column 1 indicates the Customer Name column in the database
                DuplicateName = cName;
            }
            connection.Close(); // Close the database connection
        }

        public void setCustRef() // Method for setting the auto increment Customer Reference
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT MAX(CustRef) FROM Customer;"; // SQL insert query to database (Find the biggest value CustRef already in the database)
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                if (reader.IsDBNull(0))
                {

                }
                else
                {
                    int Ref = reader.GetInt32(0);
                    this.CustRef = this.CustRef + Ref;
                }
            }
            connection.Close(); // Close the database connection
        }

        public void AddCust() // Method for adding a new customer to the database
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "INSERT INTO Customer (CustRef, Name, Age, Address) VALUES ('"+CustRef+"', '" + Name + "', '" + Age + "', '" + Address + "');"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }

        public void updateCustomer() // Method for amending the customer details which are held on the database
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "UPDATE Customer SET Name = '" + Name + "', Age = '" + Age + "', Address = '" + Address + "' WHERE Name = '" + OldName + "';"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }

        public void deleteCustomer() // Method for deleting a customer record in the database
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "DELETE FROM Customer WHERE Name = '" + OldName + "';"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }
    }
}