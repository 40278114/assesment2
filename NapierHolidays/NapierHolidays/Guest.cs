﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the Guest class which implement a Singleton Design Pattern. 
    *  
    *  It will hold the properties for the guest's passport numnber
    *  
    *  Last Updated 30/11/16 23:24
    */

    public class Guest // Inherits Name & Age from Person Class
    {
        private static Guest instance;

        private Guest() { }

        public int BookingRef { get; set; } // auto get set for booking reference
        public string guest1Name { get; set; } // auto get set for Guest 1's name
        public int? guest1Age { get; set; } // auto get set for Guest 1's age
        public string guest1PassNum { get; set; } // auto get set for Guest 1's passport number
        public string guest2Name { get; set; } 
        public int? guest2Age { get; set; } 
        public string guest2PassNum { get; set; }
        public string guest3Name { get; set; }
        public int? guest3Age { get; set; }
        public string guest3PassNum { get; set; }
        public string guest4Name { get; set; }
        public int? guest4Age { get; set; }
        public string guest4PassNum { get; set; }

        public int? totalGuests;
        public int? totalTeenGuests;

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command; // create a new sql command object

        public static Guest Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Guest();
                }
                return instance;
            }

        }

        public void addGuests() // Method for adding a guest to the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "INSERT INTO Guests (BookingRef, G1Name, G1Age, G1PassNum, G2Name, G2Age, G2PassNum, G3Name, G3Age, G3PassNum, G4Name, G4Age, G4PassNum) VALUES ('" + BookingRef + "', '" + guest1Name + "', '" + guest1Age + "', '" + guest1PassNum + "', '" + guest2Name + "', '" + guest2Age + "', '" + guest2PassNum + "', '" + guest3Name + "', '" + guest3Age + "', '" + guest3PassNum + "' , '" + guest4Name + "', '" + guest4Age + "', '" + guest4PassNum + "');"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }

        public void resetGuests() // Method to reset properties of guests to null
        {
            guest1Name = null;
            guest1Age = null;
            guest1PassNum = null;
            guest2Name = null;
            guest2Age = null;
            guest2PassNum = null;
            guest3Name = null;
            guest3Age = null;
            guest3PassNum = null;
            guest4Name = null;
            guest4Age = null;
            guest4PassNum = null;
            totalGuests = null;
        }

        public void updateGuests() // Method for amending the extras details which are held on the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "UPDATE Guests SET G1Name = '"+guest1Name+"', G1Age = '"+guest1Age+"', G1PassNum = '"+guest1PassNum+"', G2Name = '"+guest2Name+"', G2Age = '"+guest2Age+"', G2PassNum = '"+guest2PassNum+"', G3Name = '"+guest3Name+"', G3Age = '"+guest3Age+"', G3PassNum = '"+guest3PassNum+"', G4Name = '"+guest4Name+"', G4Age = '"+guest4Age+"', G4PassNum = '"+guest4PassNum+"' WHERE BookingRef = '" + BookingRef + "';"; // SQL update query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }

        public void deleteGuests() // Method for deleting the extras details which are held on the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "DELETE FROM Guests WHERE BookingRef = '" + BookingRef + "';"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }

        public void countGuests() // Method to count the number of guests for any one booking (Aged 18 and above)
        {
            int _g1 = 0;
            int _g2 = 0;
            int _g3 = 0;
            int _g4 = 0;

            if (guest1Name == null || guest1Name == "N/A")
            {
                _g1 = 0;
            }
            else
            {
                if (guest1Age >= 18)
                {
                    _g1 = 1;
                }
                else
                {
                    _g1 = 0;
                }
            }
            if (guest2Name == null || guest2Name == "N/A")
            {
                _g2 = 0;
            }
            else
            {
                if (guest2Age >= 18)
                {
                    _g2 = 1;
                }
                else
                {
                    _g2 = 0;
                }
            }
            if (guest3Name == null || guest3Name == "N/A")
            {
                _g3 = 0;
            }
            else
            {
                if (guest3Age >= 18)
                {
                    _g3 = 1;
                }
                else
                {
                    _g3 = 0;
                }
            }
            if (guest4Name == null || guest4Name == "N/A")
            {
                _g4 = 0;
            }
            else
            {
                if (guest4Age >= 18)
                {
                    _g4 = 1;
                }
                else
                {
                    _g4 = 0;
                }
            }

            this.totalGuests = (_g1 + _g2 + _g3 + _g4);
        }

        public void countTeenGuests() // Method to count the number of guests for any one booking (Below 18 years old)
        {
            int _gt1 = 0;
            int _gt2 = 0;
            int _gt3 = 0;
            int _gt4 = 0;

            if (guest1Name == null || guest1Name == "N/A")
            {
                _gt1 = 0;
            }
            else
            {
                if (guest1Age >=1 && guest1Age < 18)
                {
                    _gt1 = 1;
                }
                else
                {
                    _gt1 = 0;
                }
            }
            if (guest2Name == null || guest2Name == "N/A")
            {
                _gt2 = 0;
            }
            else
            {
                if (guest2Age >= 1 && guest2Age < 18)
                {
                    _gt2 = 1;
                }
                else
                {
                    _gt2 = 0;
                }
            }
            if (guest3Name == null || guest3Name == "N/A")
            {
                _gt3 = 0;
            }
            else
            {
                if (guest3Age >= 1 && guest3Age < 18)
                {
                    _gt3 = 1;
                }
                else
                {
                    _gt3 = 0;
                }
            }
            if (guest4Name == null || guest4Name == "N/A")
            {
                _gt4 = 0;
            }
            else
            {
                if (guest4Age >= 1 && guest4Age < 18)
                {
                    _gt4 = 1;
                }
                else
                {
                    _gt4 = 0;
                }
            }

            this.totalTeenGuests = (_gt1 + _gt2 + _gt3 + _gt4);
        }
    }
}
