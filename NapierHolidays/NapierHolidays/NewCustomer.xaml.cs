﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite; // For SQLite database
using System.Text.RegularExpressions; // For Regex validation

namespace NapierHolidays
{

    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the NewCustomer window which is opened from the MainWindow. 
    *  
    *  It allows the user to enter details of a new Customer. The details are then saved to the SQLite database. 
    *  
    *  Last Updated 30/11/16 23:24
    */

    public partial class NewCustomer : Window
    {
        public NewCustomer()
        {
            InitializeComponent();
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e) // When clicked, will validate fields first, if okay then call saveCust method to save details to database
        {
            Regex namePattern = new Regex(@"^[A-Za-z\s]*\z$"); // Name entered must be alphabetical characters only (a-z & A-Z)
            Regex agePattern = new Regex(@"^[0-9]{1,2}\z$"); // Age entered must be numeric characters only (0-9) and may only be two charaters long

            if (nameBox.Text == string.Empty || ageBox.Text == string.Empty || addressBox.Text == string.Empty) // Initial check to makle sure all text boxes have been populated
            {
                MessageBox.Show("You must enter the customer's name, age and address"); // If empty, user will receive this message
                return;
            }
            if (namePattern.IsMatch(nameBox.Text) == false)
            {
                MessageBox.Show("Customer's name is not valid. Name must only contain alphabetical characters (a-z & A-Z)"); // If Regex pattern & entered string DO NOT match - user get's this message
                return;
            }
            if (agePattern.IsMatch(ageBox.Text) == false)
            {
                MessageBox.Show("Customer's age is not valid. Age must be in numeric format & can only be two characters long"); // If Regex pattern & entered string DO NOT match - user get's this message
                return;
            }
            else
            {
                saveCust();
            }
        }

        private void saveCust()
        {
            Customer newCust = Customer.Instance; // new Customer Singleton object 
            newCust.setCustRef(); // Sets auto increment customer reference
            newCust.Name = nameBox.Text; // Sets customer name
            newCust.Age = Convert.ToInt32(ageBox.Text);
            newCust.Address = addressBox.Text; // Sets customer address
            newCust.checkforDuplicates(); // This method will check for duplicate customers
            if (newCust.Name == newCust.DuplicateName)
            {
                MessageBox.Show("This customer already exsists on our database");
            }
            else
            {
                newCust.AddCust(); // Method in Customer class which adds the customer to the database
                MessageBox.Show("Customer details saved"); // Message box shown to user
                this.Close(); // Closes the window after customer details have been saved
            }
        }
    }
}
