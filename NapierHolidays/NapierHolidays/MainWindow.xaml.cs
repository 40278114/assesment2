﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This GUI (MainWindow) will have options to create, view & amend Customer & Booking information. 
    *  
    *  Last Updated 30/11/16 23:24
    */

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection

        private void CreateCust_Click(object sender, RoutedEventArgs e) // When clicked, will open NewCustomer window to add a new customer details.
        {
            NewCustomer newCustWin = new NewCustomer(); 
            newCustWin.ShowDialog();
            showCustomers();
        }

        private void viewCust_Click(object sender, RoutedEventArgs e) // When clicked, will show Customer data from the database on the data grid.
        {
            showCustomers();
        }

        private void CreateBooking_Click(object sender, RoutedEventArgs e) // When clicked, will open NewBooking window to add a new booking details.
        {
            NewBooking newBookWin = new NewBooking();
            newBookWin.ShowDialog();
            showBookings();
        }

        private void viewBookings_Click(object sender, RoutedEventArgs e) // When clicked, will show Bookings data from the database on the data grid.
        {
            showBookings();
        }

        public void showCustomers() // Method for showing customer data from the database in the datagrid
        {
            connection.Open(); // opens the connection to database
            string Query = "SELECT CustRef AS Customer_Reference, Name, Age, Address FROM Customer"; // SQL query to database
            SQLiteCommand command = new SQLiteCommand(Query, connection);
            command.ExecuteNonQuery();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
            DataTable table = new DataTable("Customer");
            adapter.Fill(table);
            DatabaseGrid.ItemsSource = table.DefaultView;
            adapter.Update(table);
            connection.Close(); // closes the database connection
        }

        public void showBookings() // Method for showing booking data from the database in the datagrid
        {
            connection.Open(); // opens the connection to database
            string Query = "SELECT BookingRef, Name, ArrivalDate, DepartDate, Nights FROM Booking JOIN Customer ON Booking.CustRef = Customer.CustRef"; // SQL query to database
            SQLiteCommand command = new SQLiteCommand(Query, connection);
            command.ExecuteNonQuery();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
            DataTable table = new DataTable("Booking");
            adapter.Fill(table);
            DatabaseGrid.ItemsSource = table.DefaultView;
            adapter.Update(table);
            connection.Close(); // closes the database connection
        }

        private void amendCust_Click(object sender, RoutedEventArgs e) // When clicked show amend customer window
        {
            AmendCustomer amendCust = new AmendCustomer();
            amendCust.ShowDialog();
            showCustomers(); // When closed, update the customer table again
        }

        private void amendBooking_Click(object sender, RoutedEventArgs e) // When clicked show amend booking window
        {
            AmendBooking amendBooking = new AmendBooking();
            amendBooking.ShowDialog();
            showBookings(); // When closed update the booking table again
        }

        private void invoiceBtn_Click(object sender, RoutedEventArgs e) // When clicked show invoice window
        {
            Invoice invoice = new Invoice();
            invoice.ShowDialog();
        }
    }
}
