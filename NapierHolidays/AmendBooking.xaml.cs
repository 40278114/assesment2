﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the AmendBooking window which is opened from the MainWindow. 
    *  
    *  It allows the user to Amend or Delete a Booking
    *  
    *  Last Updated 28/11/16 20:02
    */

    public partial class AmendBooking : Window
    {
        public AmendBooking()
        {
            InitializeComponent();
            fill_CustCombo();
        }

        string _custName; // Local variable used for findCustRef method
        int _custReference; // Local variable used for finding the booking details from the database
        int _bookRef; // Local variable used for storing the booking reference number

        string sArrival; // Local variable used for storing the arrival date in string format
        DateTime _arrival; // Local variable used for storing the arrival date
        DateTime _depart; // Local variable used for storing the departure date
        int _nights; // Local variable used for storing the number of nights

        string _g1Name;
        int _g1Age;
        int _g1PassNum;
        string _g2Name;
        int _g2Age;
        int _g2PassNum;
        string _g3Name;
        int _g3Age;
        int _g3PassNum;
        string _g4Name;
        int _g4Age;
        int _g4PassNum;

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command; // creates new database command

        private void custCombo_DropDownClosed(object sender, EventArgs e) // After the user selects a customer to amend, the booking combo box will populate the bookings they have made
        {
            if (custCombo.Text == String.Empty) // If the combobox is closed but missing string, show message
            {
                MessageBox.Show("Please select a customer from the dropdown box");
            }
            else // Retrieve the customer details from the database and fill in the boxes
            {
                _custName = custCombo.Text;
                findCustRef();
                fill_BookingCombo();
            }
        }

        private void custCombo_SelectionChanged(object sender, SelectionChangedEventArgs e) // If the user selects the wrong customer and chooses another, the event will automatically update the booking combo
        {
            bookingCombo.Text = String.Empty;
            bookingCombo.Items.Clear();
            _custName = custCombo.Text;
            findCustRef();
            fill_BookingCombo();
        }

        private void bookingCombo_DropDownClosed(object sender, EventArgs e)
        {
            // Fill in booking details
            getBookingDetails();
            arrivalPicker.SelectedDate = this._arrival;
            departPicker.SelectedDate = this._depart;
            nightsNum.Text = Convert.ToString(this._nights);

            // Fill in guest details
            getGuestDetails();
            name1Box.Text = _g1Name;
            age1Box.Text = Convert.ToString(_g1Age);
            passport1Box.Text = Convert.ToString(_g1PassNum);
            name2Box.Text = _g2Name;
            age2Box.Text = Convert.ToString(_g2Age);
            passport2Box.Text = Convert.ToString(_g2PassNum);
            name3Box.Text = _g3Name;
            age3Box.Text = Convert.ToString(_g3Age);
            passport3Box.Text = Convert.ToString(_g3PassNum);
            name4Box.Text = _g4Name;
            age4Box.Text = Convert.ToString(_g4Age);
            passport4Box.Text = Convert.ToString(_g4PassNum);
        }

        private void fill_CustCombo() // This method will fill the custCombo with Customer names from the database. This will identify which customer the booking is for.  
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Customer"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string Name = reader.GetString(1); 
                custCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        private void fill_BookingCombo() // This method will fill the booking combo box with arrival dates. This is handy if the customer has more than one booking.
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Booking WHERE CustRef = '" + this._custReference + "';"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                sArrival = reader.GetString(2);
                bookingCombo.Items.Add("Arriving on: " + sArrival); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        private void findCustRef() // Method to find the customer reference in the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT CustRef FROM Customer WHERE Name = '" + this._custName + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                this._custReference = reader.GetInt32(0); 
            }
            connection.Close(); // Close the database connection
        }

        private void getBookingDetails() // Method to get booking details from the database 
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Booking WHERE CustRef = '" + this._custReference + "' AND ArrivalDate = '" + this.sArrival + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                this._bookRef = reader.GetInt32(0);
                this._custReference = reader.GetInt32(1);
                string arrival = reader.GetString(2);
                this._arrival = DateTime.Parse(arrival);
                string depart = reader.GetString(3);
                this._depart = DateTime.Parse(depart);
                this._nights = reader.GetInt32(4);
            }
            connection.Close(); // Close the database connection
        }

        private void getGuestDetails()
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Guests WHERE BookingRef = '" + this._bookRef + "';";
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                // Guest 1
                this._g1Name = reader.GetString(1);
                this._g1Age = reader.GetInt32(2);
                this._g1PassNum = reader.GetInt32(3);

                // Guest 2
                this._g2Name = reader.GetString(4);
                this._g2Age = reader.GetInt32(5);
                this._g2PassNum = reader.GetInt32(6);

                // Guest 3
                this._g3Name = reader.GetString(7);
                this._g3Age = reader.GetInt32(8);
                this._g3PassNum = reader.GetInt32(9);               

                // Guest 4
                this._g4Name = reader.GetString(10);
                this._g4Age = reader.GetInt32(11);
                this._g4PassNum = reader.GetInt32(12);
            }
            connection.Close(); // Close the database connection
        }

        private void getExtrasDetails()
        {

        }
    }
}
