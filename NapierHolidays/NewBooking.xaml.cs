﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace NapierHolidays
{
    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the NewBooking window which is opened from the MainWindow. 
    *  
    *  It allows the user to enter details of a new Booking (including the guests & extras). The details are then saved to the SQLite database. 
    *  
    *  Last Updated 28/11/16 20:02
    */

    public partial class NewBooking : Window
    {
        public NewBooking()
        {
            InitializeComponent();
            fill_CustCombo();
            fill_DriverCombo();
        }

        void fill_CustCombo() // This method will fill the custCombo with Customer names from the database. This will identify which customer the booking is for.  
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Customer"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while(reader.Read())
            {
                string Name = reader.GetString(1); // Column 1 indicates the Customer Name column in the SQLite database
                custCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        void fill_DriverCombo() // This method will fill the driverCombo with Driver names from the database.
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
            SQLiteCommand command;
            connection.Open(); // open the connection:
            command = connection.CreateCommand(); // create a new SQL command:
            command.CommandText = "SELECT * FROM Drivers"; // SQL insert query to database
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                string Name = reader.GetString(0); // Column 1 indicates the Customer Name column in the SQLite database
                driverCombo.Items.Add(Name); // Add details to the combo box
            }
            connection.Close(); // Close the database connection
        }

        private void departPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates the amount of nights based on two dates entered by the user
        {
            DateTime aDate = arrivalPicker.SelectedDate.Value; // Gets departure date value 
            DateTime dDate = departPicker.SelectedDate.Value; // Gets departure date value
            TimeSpan ts = dDate - aDate; // Calculates the difference in days
            int differenceInDays = ts.Days;
            nightsNum.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
        }

        private void endPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e) // This method calculates the number of days the care will be hired for
        {
            DateTime sDate = startPicker.SelectedDate.Value; // Gets departure date value 
            DateTime eDate = endPicker.SelectedDate.Value; // Gets departure date value
            TimeSpan ts = eDate - sDate; // Calculates the difference in days
            int differenceInDays = ts.Days;
            hireDays.Text = Convert.ToString(differenceInDays); // Displays the result in the Textblock
        }

        public void saveBtn_Click(object sender, RoutedEventArgs e) // When the Save button is clicked, the booking details will be added to the database
        {
            // Booking table information for database
            Booking newBooking = Booking.Instance; // new Booking object
            newBooking.CustName = custCombo.Text; // Sets the customer name on the booking (taken from the combo box)
            newBooking.findCustRef(); // Finds the customer reference
            newBooking.arrivalDate = arrivalPicker.SelectedDate.Value; // Sets booking arrival date 
            newBooking.departureDate = departPicker.SelectedDate.Value; // Sets booking departure date
            newBooking.Nights = Convert.ToInt32(nightsNum.Text); // Sets the amount of nights for the booking
            newBooking.AddBooking(); // Adds the booking details to the database
            newBooking.findBookingRef(); // Gets the booking reference

            // Guests table information for database
            if (name1Box.Text != String.Empty || name2Box.Text != String.Empty || name3Box.Text != String.Empty || name4Box.Text != String.Empty)
            {
                Guest addGuest = Guest.Instance;
                // Add Guest 1
                if (name1Box.Text != String.Empty || age1Box.Text != String.Empty || passport1Box.Text != String.Empty) // If boxes are NOT empty then set guest 1
                {
                    addGuest.BookingRef = newBooking.BookingRef;
                    addGuest.guest1Name = name1Box.Text;
                    addGuest.guest1Age = Convert.ToInt32(age1Box.Text);
                    addGuest.guest1PassNum = Convert.ToInt32(passport1Box.Text);
                }
                else
                {
                    addGuest.guest1Name = "N/A";
                    addGuest.guest1Age = 0;
                    addGuest.guest1PassNum = 0;
                }
                // Add Guest 2
                if (name2Box.Text != String.Empty || age2Box.Text != String.Empty || passport2Box.Text != String.Empty) // If boxes are NOT empty then set guest 2
                {
                    addGuest.guest2Name = name2Box.Text;
                    addGuest.guest2Age = Convert.ToInt32(age2Box.Text);
                    addGuest.guest2PassNum = Convert.ToInt32(passport2Box.Text);
                }
                else
                {
                    addGuest.guest2Name = "N/A";
                    addGuest.guest2Age = 0;
                    addGuest.guest2PassNum = 0;
                }
                // Add Guest 3
                if (name3Box.Text != String.Empty || age3Box.Text != String.Empty || passport3Box.Text != String.Empty) // If boxes are NOT empty then set guest 3
                {
                    addGuest.guest3Name = name3Box.Text;
                    addGuest.guest3Age = Convert.ToInt32(age3Box.Text);
                    addGuest.guest3PassNum = Convert.ToInt32(passport3Box.Text);
                }
                else
                {
                    addGuest.guest3Name = "N/A";
                    addGuest.guest3Age = 0;
                    addGuest.guest3PassNum = 0;
                }
                // Add Guest 4
                if (name4Box.Text != String.Empty || age4Box.Text != String.Empty || passport4Box.Text != String.Empty) // If boxes are NOT empty then set guest 4
                {
                    addGuest.guest4Name = name4Box.Text;
                    addGuest.guest4Age = Convert.ToInt32(age4Box.Text);
                    addGuest.guest4PassNum = Convert.ToInt32(passport4Box.Text);
                }
                else
                {
                    addGuest.guest4Name = "N/A";
                    addGuest.guest4Age = 0;
                    addGuest.guest4PassNum = 0;
                }
                addGuest.addGuestDetails(); // Method to add guest details to database (See booking class)
            }

            // Extras table information for database
            if (breakfastBox.IsChecked.HasValue && breakfastBox.IsChecked.Value || eveningBox.IsChecked.HasValue && eveningBox.IsChecked.Value || hireBox.IsChecked.HasValue && hireBox.IsChecked.Value) // If any of these checkboxes are checked then proceed
            {
                Extras extras = Extras.Instance; // Create Extras Singleton object
                extras.BookingRef = newBooking.BookingRef;
                extras.dietRequirements = dietBox.Text;

                if (breakfastBox.IsChecked.HasValue && breakfastBox.IsChecked.Value) // If checked then set string to Yes
                {
                    extras.breakfastMeals = "Yes";
                }
                else
                {
                    extras.breakfastMeals = "No";
                }

                if (eveningBox.IsChecked.HasValue && eveningBox.IsChecked.Value)
                {
                    extras.eveningMeals = "Yes";
                }
                else
                {
                    extras.eveningMeals = "No";
                }

                if (hireBox.IsChecked.HasValue && hireBox.IsChecked.Value) 
                {
                    extras.carHire = "Yes";
                    extras.hireStart = startPicker.SelectedDate.Value;
                    extras.hireEnd = endPicker.SelectedDate.Value;
                    extras.driverName = driverCombo.Text;
                    extras.hireDays = Convert.ToInt32(hireDays.Text);
                }
                else
                {
                    extras.carHire = "No";
                }
                extras.addExtras(); // Method to add extras to database (See extras class)
            }

            MessageBox.Show("Booking details saved"); // Message box shown to user
            this.Close(); // Closes the window after booking details have been saved
        }
    }
}
