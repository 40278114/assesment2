﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace NapierHolidays
{

    /* Napier Holiday System created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This is the Booking Class which has been implemented using the Singleton Design Pattern. 
    *  
    *  It will hold the properties for the booking.
    *  
    *  Last Updated 28/11/16 20:02
    */

    public class Booking
    {
        private static Booking instance;

        private Booking() { }

        public string CustName { get; set; } // auto get set for customer reference
        public int CustRef { get; set; } // auto get set for customer reference
        public int BookingRef { get; set; } // auto get set for booking reference
        public DateTime arrivalDate { get; set; } // auto get set for arrival date
        public DateTime departureDate { get; set; } // auto get set for departutre date
        public int Nights { get; set; } // auto get set for nights

        SQLiteConnection connection = new SQLiteConnection("Data Source=HolidaysDatabase.db;Version=3;"); // creates new database connection
        SQLiteCommand command; // create a new sql command object

        public static Booking Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Booking();
                }
                return instance;
            }

        }

        public void findCustRef() // Method to find the customer reference in the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT CustRef FROM Customer WHERE Name = '"+CustName+"'"; 
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the customer data in the database
            while (reader.Read())
            {
                int CustRef = reader.GetInt32(0); // Column 0 indicates the Customer Name column in the SQLite database
                this.CustRef = CustRef;
            }
            connection.Close(); // Close the database connection
        }

        public void findBookingRef() // Method to find the booking reference in the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "SELECT BookingRef FROM Booking WHERE CustRef = '" + CustRef + "'"; 
            SQLiteDataReader reader = command.ExecuteReader(); // The reader will read the booking data in the database
            while (reader.Read())
            {
                int Ref = reader.GetInt32(0); // Column 1 indicates the Customer Name column in the SQLite database
                this.BookingRef = Ref;
            }
            connection.Close(); // Close the database connection
        }

        public void AddBooking() // Method for adding a new customer to the database
        {
            connection.Open(); // open the connection:
            command = connection.CreateCommand();
            command.CommandText = "INSERT INTO Booking (CustRef, ArrivalDate, DepartDate, Nights) VALUES ('" + CustRef + "', '" + arrivalDate.ToShortDateString() + "', '" + departureDate.ToShortDateString() + "', '" + Nights + "');"; // SQL insert query to database
            command.ExecuteNonQuery(); // Execute the SQL command
            connection.Close(); // Close the database connection
        }
    }
}